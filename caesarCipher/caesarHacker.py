__author__ = 'lkreitmann'

LETTERS = ' !@#$%^&*()_+-=`~1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,./<>?;\':"[]\\{}|'

message = input('What is your encrypted message? ')

for key in range(len(LETTERS)):
    translated = ''
    for char in message:
        if char in LETTERS:
            num = LETTERS.find(char)
            num -= key
            if num < 0:
                num += len(LETTERS)
            translated += LETTERS[num]
        else:
            translated = translated + char
    if 'the' in translated.lower() or \
            'so' in translated.lower() or \
            'it' in translated.lower() or \
            'me' in translated.lower() or \
            'no' in translated.lower() or \
            'my' in translated.lower() or \
            'no' in translated.lower() or \
            'in' in translated.lower() or \
            'of' in translated.lower() or \
            'out' in translated.lower() or \
            'is' in translated.lower() or \
            'and' in translated.lower() or \
            'at' in translated.lower():
        print('Key #%s: %s' % (key, translated))
