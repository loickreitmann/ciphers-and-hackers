__author__ = 'lkreitmann'
# Caesar Cipher
import pyperclip

message = input('What is your message? ')
key = input('what is your key? ')
mode = input('Encode? (Y/n) ')
crypt = False
if mode == '' or mode.lower() == 'y':
    crypt = True
key = int(key)
LETTERS = ' !@#$%^&*()_+-=`~1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,./<>?;\':"[]\\{}|'

translated = ''

for char in message:
    if char in LETTERS:
        num = LETTERS.find(char)
        if crypt:
            num += key
        else:
            num -= key
        if num >= len(LETTERS):
            num -= len(LETTERS)
        elif num < 0:
            num += len(LETTERS)
        translated += LETTERS[num]
    else:
        translated += char

print(translated)

pyperclip.copy(translated)

