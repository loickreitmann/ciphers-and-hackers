import math
import pyperclip
__author__ = 'lkreitmann'


def main():
    message = input('Enter your ecrypeted message: ')
    key = input('Enter your key: ')
    if key.isdigit():
        key = int(key)
    else:
        key = 0
    plaintext = decrypt_message(key, message)
    print(plaintext)
    pyperclip.copy(plaintext)


def decrypt_message(key, message):
    columns = math.ceil(len(message) / key)
    rows = key
    shadows = columns * rows - len(message)
    decrypted_text = [''] * columns
    col, row = 0, 0
    for char in message:
        decrypted_text[col] += char
        col += 1
        if col == columns or (col == columns - 1 and row >= rows - shadows):
            col = 0
            row += 1
    return ''.join(decrypted_text)


if __name__ == '__main__':
    main()


# 'H cb  irhdeuousBdi   prrtyevdgp nir  eerit eatoreechadihf paken ge b te dih aoa.da tts tn'
# 9 => He picked up the acorn and buried it straight By the side of a river both deep and great.
# 'A b  drottthawa nwar eci t nlel ktShw leec,hheat .na  e soogmah a  ateniAcgakh dmnor  '
# 9 => At length he came back, and with him a She And the acorn was grown to a tall oak tree.
# 'Bmmsrl dpnaua!toeboo'ktn uknrwos. yaregonr w nd,tu  oiady hgtRwt   A hhanhhasthtev  e t e  eo'
# 9 => But with many a hem! and a sturdy stroke, At length he brought down the poor Raven's own oak.
