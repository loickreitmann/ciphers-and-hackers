__author__ = 'lkreitmann'


def main():
    message = input('What is your message? ')
    key = input('What is your key? ')
    while not key.isdigit():
        key = input('Enter a numeric key: ')
    key = int(key)
    print('[', encrypt_message(key, message), ']')


def encrypt_message(key, message):
    translated = [''] * key
    for col in range(key):
        index = col
        while index < len(message):
            translated[col] += message[index]
            index += key
    return ''.join(translated)

if __name__ == '__main__':
    main()

