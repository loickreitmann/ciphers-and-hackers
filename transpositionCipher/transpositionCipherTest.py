import random
import sys
import math
import transpositionCipherEncrypter
import transpositionCipherDecrypter

__author__ = 'lkreitmann'


def main():
    random.seed(39)
    for i in range(20):
        # generate random message
        # of random length
        message = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890' * random.randint(4, 40)
        # convert the string into a list
        message = list(message)
        # shufflethe list
        random.shuffle(message)
        # set the message to be encrypted
        message = ''.join(message)
        loopcount = 0
        for key in range(1, math.floor(len(message)/5)):
            encrypted = transpositionCipherEncrypter.encrypt_message(key, message)
            decrypted = transpositionCipherDecrypter.decrypt_message(key, encrypted)
            if message != decrypted:
                print('Mismatch with key %s and message "%s".' % (key, message))
                print(decrypted)
                sys.exit()
            loopcount = key
        print('Test #%s: message "%s..." encrypted and decrypted %s times: PASSED!' % (i+1, message[:50], loopcount))
    print('Transposition Cipher tests passed!')


if __name__ == '__main__':
    main()
